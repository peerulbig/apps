/****************************************************************************
 * examples/vamex_control/vamex_control.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>
#include <mqueue.h>
#include <nuttx/timers/timer.h>
#include <semaphore.h>


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define CONFIG_VAMEX_CONTROL_DEVNAME "/dev/timer8"
#define CONFIG_VAMEX_CONTROL_INTERVAL 10000
#define CONFIG_VAMEX_CONTROL_SIGNO 17
#define CONFIG_VAMEX_CONTROL_DELAY 10000

#define CHILD_ARG ((void*)0x12345678)

int child_apid;


/****************************************************************************
 * Public Functions
 ****************************************************************************/
static volatile unsigned long g_nsignals;
static sem_t *sem_vamex_control;

/****************************************************************************
 * timer_sighandler
 ****************************************************************************/


static void timer_sighandler(int signo, FAR siginfo_t *siginfo,
                             FAR void *context)
{
  /* Does nothing in this example except for increment a count of signals
   * received.
   *
   * NOTE: The use of signal handler is not recommended if you are concerned
   * about the signal latency.  Instead, a dedicated, high-priority thread
   * that waits on sigwaitinfo() is recommended.  High priority is required
   * if you want a deterministic wake-up time when the signal occurs.
   */
//printf("SIG_HANDLER active\n");
/* printf("return: %x", pid_1);
 kill(pid_1, 17);
 kill(5,18);*/

//uncomment to trigger task waiting for sem_vamex_conrtol
//sem_post(sem_vamex_control);

 g_nsignals++;
}


/****************************************************************************
 * vamex_control_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int vamex_control_main(int argc, char *argv[])
#endif
{
  struct timer_notify_s notify;
  struct sigaction act;
  struct timer_status_s status;
  int ret;
  int fd;

  //sigo =17;


  pthread_attr_t attr;
  pthread_t thread;



  nutros_a_main();


  /* Open the timer device */

  printf("Open %s\n", CONFIG_VAMEX_CONTROL_DEVNAME);

  fd = open(CONFIG_VAMEX_CONTROL_DEVNAME, O_RDONLY);
  if (fd < 0)
    {
	  fprintf(stderr, "ERROR: Failed to open %s: %d\n",
			  CONFIG_VAMEX_CONTROL_DEVNAME, errno);
      return EXIT_FAILURE;
    }



    /* Set the timer interval */

    printf("Set timer interval to %lu\n",
           (unsigned long)CONFIG_VAMEX_CONTROL_INTERVAL);

    ret = ioctl(fd, TCIOC_SETTIMEOUT, CONFIG_VAMEX_CONTROL_INTERVAL);
    if (ret < 0)
      {
        fprintf(stderr, "ERROR: Failed to set the timer interval: %d\n", errno);
        close(fd);
        return EXIT_FAILURE;
      }
  //------------------------------------------------------------------------------------------
    //SIGACTION
    g_nsignals       = 0;
    act.sa_sigaction = timer_sighandler;
    act.sa_flags     = SA_SIGINFO;

    (void)sigfillset(&act.sa_mask);
    (void)sigdelset(&act.sa_mask, CONFIG_VAMEX_CONTROL_SIGNO);

    ret = sigaction(CONFIG_VAMEX_CONTROL_SIGNO, &act, NULL);
    if (ret != OK)
      {
        fprintf(stderr, "ERROR: Fsigaction failed: %d\n", errno);
        close(fd);
        return EXIT_FAILURE;
      }

    printf("Attach timer handler\n");

    //timer action

     notify.arg   = NULL;
     notify.pid   = getpid();
     notify.signo = CONFIG_VAMEX_CONTROL_SIGNO;

     ret = ioctl(fd, TCIOC_NOTIFICATION, (unsigned long)((uintptr_t)&notify));
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to set the timer handler: %d\n", errno);
         close(fd);
         return EXIT_FAILURE;
       }


     /* Start the timer */

     printf("Start the timer\n");

     ret = ioctl(fd, TCIOC_START, 0);
     if (ret < 0)
       {
         fprintf(stderr, "ERROR: Failed to start the timer: %d\n", errno);
         close(fd);
         return EXIT_FAILURE;
       }



     sem_vamex_control = sem_open("sync_qeadc",O_CREAT);
     printf("ret sem_1 qeadc: %i \n", sem_vamex_control);
     /*warten*/
     sleep(2);

     qeadc_main();

     printf("qeadc_main returned\n");

     for(;;){
    	 sleep(10);
     }
     //for(;;){
    	/* printf("  flags: %08lx timeout: %lu timeleft: %lu nsignals: %lu\n",
    	          (unsigned long)status.flags, (unsigned long)status.timeout,
    	          (unsigned long)status.timeleft, g_nsignals);
    	*/
    	 //pthread_kill(pid_1,  sigo);

    	// 	 usleep(CONFIG_VAMEX_CONTROL_DELAY);
     //}


  return 0;
}
