/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Auxiliary.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_NODESTATUS_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_NODESTATUS_H_

#include "../../../../sensor/lib/uavcan/datatype/Data.h"

namespace uavcan {
struct node_status_s {
	uint32_t time;
	uint8_t health;
	uint8_t mode;
	uint8_t subMode;
	uint16_t vendorStatus;
};

class NodeStatus: public Data {
  static const uint16_t DATATYPEID = 341;
  static const uint64_t SIGNATURE = 0xf0868d0c1a7c6f1UL;
  static const bool SERVICE = false;

  void setUptime(uint32_t time);
  void setHealth(uint8_t health);
  void setMode(uint8_t mode);
  void setSubMode(uint8_t subMode);
  void setVendorSpecificStatus(uint16_t status);


public:
  static uint16_t getDatatype()
    {
      return DATATYPEID;
    }
  NodeStatus(node_status_s &node_data);

  virtual ~NodeStatus();
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_NODESTATUS_H_ */


//
//#
//# Abstract node status information.
//#
//# Any UAVCAN node is required to publish this message periodically.
//#
//
//#
//# Publication period may vary within these limits.
//# It is NOT recommended to change it at run time.
//#
//uint16 MAX_BROADCASTING_PERIOD_MS = 1000
//uint16 MIN_BROADCASTING_PERIOD_MS = 2
//
//#
//# If a node fails to publish this message in this amount of time, it should be considered offline.
//#
//uint16 OFFLINE_TIMEOUT_MS = 3000
//
//#
//# Uptime counter should never overflow.
//# Other nodes may detect that a remote node has restarted when this value goes backwards.
//#
//uint32 uptime_sec
//
//#
//# Abstract node health.
//#
//uint2 HEALTH_OK         = 0     # The node is functioning properly.
//uint2 HEALTH_WARNING    = 1     # A critical parameter went out of range or the node encountered a minor failure.
//uint2 HEALTH_ERROR      = 2     # The node encountered a major failure.
//uint2 HEALTH_CRITICAL   = 3     # The node suffered a fatal malfunction.
//uint2 health
//
//#
//# Current mode.
//#
//# Mode OFFLINE can be actually reported by the node to explicitly inform other network
//# participants that the sending node is about to shutdown. In this case other nodes will not
//# have to wait OFFLINE_TIMEOUT_MS before they detect that the node is no longer available.
//#
//# Reserved values can be used in future revisions of the specification.
//#
//uint3 MODE_OPERATIONAL      = 0         # Node is performing its main functions.
//uint3 MODE_INITIALIZATION   = 1         # Node is initializing; this mode is entered immediately after startup.
//uint3 MODE_MAINTENANCE      = 2         # Node is under maintenance.
//uint3 MODE_SOFTWARE_UPDATE  = 3         # Node is in the process of updating its software.
//uint3 MODE_OFFLINE          = 7         # Node is no longer available.
//uint3 mode
//
//#
//# Not used currently, keep zero when publishing, ignore when receiving.
//#
//uint3 sub_mode
//
//#
//# Optional, vendor-specific node status code, e.g. a fault code or a status bitmask.
//#
//uint16 vendor_specific_status_code
