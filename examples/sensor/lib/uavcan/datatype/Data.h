/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/Data.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_DATA_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_DATA_H_

#include <nuttx/config.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

namespace uavcan {

class Data {
  union Fp32 {
    uint32_t u;
    float f;
  };
protected:
  uint8_t size;
  uint8_t *data;
  uint64_t signature;
  uint16_t dataTypeId;
  bool service;
  Data();
  Data(uint8_t _size, uint64_t _signature, bool _service, uint16_t _dataTypeId);
  int setData(void* addData, uint16_t posBegin, uint16_t posEnd);
public:
  uint8_t getSize();
  uint8_t* getData();
  uint64_t getSignature();
  uint16_t getDatatypeId();
  bool isService();
  void printData();
  virtual ~Data();

  static uint16_t getFloat16(float f32);
  static float getFloat32(uint16_t f16);
};

} /* namespace uavcan */

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_DATATYPE_DATA_H_ */
