/****************************************************************************
 * examples/sensor/lib/uavcan/datatype/MagneticFieldStrength.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/datatype/MagneticFieldStrength.h"

namespace uavcan {

MagneticFieldStrength::MagneticFieldStrength(magnet_s &mag_data)
    : Data(6 + 2 * mag_data.sizeCovariance, SIGNATURE, SERVICE, DATATYPEID)
  {
    setMagneticField(mag_data.x, mag_data.y, mag_data.z);
    setMagneticCovariance(mag_data.covariance, mag_data.sizeCovariance);
  }

void MagneticFieldStrength::setMagneticField(float magX, float magY, float magZ)
  {
    uint16_t magX16 = Data::getFloat16(magX);
    uint16_t magY16 = Data::getFloat16(magY);
    uint16_t magZ16 = Data::getFloat16(magZ);
    Data::setData((void*) &magX16, 0, 16);
    Data::setData((void*) &magY16, 16, 16);
    Data::setData((void*) &magZ16, 32, 16);
  }

void MagneticFieldStrength::setMagneticCovariance(float* magCovariance,
    uint8_t sizeMagVariance)
  {
    if (magCovariance != NULL && sizeMagVariance > 0)
      {
        for (int i = 0; i < sizeMagVariance; i++)
          {
            uint16_t magCoVar16 = Data::getFloat16(magCovariance[i]);
            Data::setData((void*) &magCoVar16, 48 + i * 16, 16);
          }
      }
  }

MagneticFieldStrength::~MagneticFieldStrength()
  {
//	printf("Delete MagnetFieldObject!\n");
  }

} /* namespace uavcan */
