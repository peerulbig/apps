/****************************************************************************
 * examples/sensor/lib/uavcan/UavcanMsg.h
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef APPS_EXAMPLES_SENSOR_LIB_UAVCAN_UAVCANMSG_H_
#define APPS_EXAMPLES_SENSOR_LIB_UAVCAN_UAVCANMSG_H_

#include <nuttx/compiler.h>

namespace uavcan {

begin_packed_struct struct uavcan_id_s {
  uint8_t node_id :7;
  bool service :1;
  uint16_t msg_type :16;
  uint8_t priority :8;
}end_packed_struct;

begin_packed_struct struct uavcan_tailByte_s {
  uint8_t transfer_id :5;
  bool toggle :1;
  bool end :1;
  bool start :1;
}end_packed_struct;

struct uavcan_payload_s {
  uavcan_payload_s()
      : size(0)
    {
    }
  uint8_t data[7];
  uint8_t size;
  struct uavcan_tailByte_s tailByte;
};

struct uavcan_msg_s {
  bool isMultiframe()
    {
      return !(payload->tailByte.end && payload->tailByte.end);
    }
  bool isStart()
    {
      return payload->tailByte.start;
    }
  bool isEnd()
    {
      return payload->tailByte.end;
    }
  bool isAnonymousTransfer()
    {
      return can_id.node_id == 0;
    }
  uint32_t getMonotonicTimestampMs()
    {
      return timestamp.tv_nsec * 1000 + timestamp.tv_nsec / 1000000;
    }
  struct uavcan_id_s can_id;
  struct uavcan_payload_s* payload;
  timespec timestamp;
  uint8_t msgCount;
};

} //namespace uavcan

#endif /* APPS_EXAMPLES_SENSOR_LIB_UAVCAN_UAVCANMSG_H_ */
