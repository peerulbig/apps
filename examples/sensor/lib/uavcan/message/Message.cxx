/****************************************************************************
 * examples/sensor/lib/uavcan/message/Message.cxx
 *
 *   Copyright (C) 2017 Jonas Mikolajczyk. All rights reserved.
 *   Author: Jonas Mikolajczyk <j.mikolajczyk@tu-braunschweig.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include "../../../../sensor/lib/uavcan/message/Message.h"

namespace uavcan {

int Message::send()
  {
    int ret;
    ret = CanDriver::getDriver()->open_uavcan();
    if (ret < 0)
      {
        return ret;
      }
    ret = CanDriver::getDriver()->write_uavcan(&msg);
    return ret;
  }

uint16_t Message::getMessageType() {
	return msg.can_id.msg_type;
}

void Message::print(bool detail)
  {
    if (detail)
      {
        printf("\n  _______________________\n");
        printf("  |                      |\n");
        printf("  |    DataType: %04i    |\n", msg.can_id.msg_type);
        printf("  |    Priority:   %02i    |\n", msg.can_id.priority);
        printf("  |    Service :    %01i    |\n", msg.can_id.service);
        printf("  |    NodeId  :   %02i    |\n", msg.can_id.node_id);
        printf("  |    MsgCount:   %02i    |\n", msg.msgCount);
        printf("  |    Transfer:   %02i    |\n",
            msg.payload[0].tailByte.transfer_id);
        printf("  |    Payload :   %02i    |\n", msg.payload[0].size);
        printf("  |______________________|\n\n");
      }
    printf(
        "   _____|__01__|__02__|__03__|__04__|__05__|__06__|__07__|__Tail__\n");
    for (int i = 0; i < msg.msgCount; i++)
      {
        printf("%02i:  %02i ", msg.payload[i].size, i * 7);
        for (int j = 0; j < msg.payload[i].size; j++)
          {
            printf("|  %02x  ", msg.payload[i].data[j]);
          }
        if (i == msg.msgCount - 1)
          {
            for (int k = 0; k < (7 - msg.payload[i].size); k++)
              {
                printf("|      ");
              }
          }
        printf("|   %02x\n", msg.payload[i].tailByte);
      }
    printf("\n");
  }

Message::~Message()
  {
    delete (msg.payload);
    msg.payload = NULL;
  }

} /* namespace uavcan */

