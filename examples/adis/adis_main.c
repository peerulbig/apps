/****************************************************************************
 * examples/hello/hello_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <nuttx/sensors/adis16xxx.h>
#include <fcntl.h>
#include <sys/ioctl.h>
/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int adis_main(int argc, char *argv[])
#endif
{
	int fd;
	int32_t iax;
	int32_t iay;
	int32_t iaz;
	//double ax;
	//double ay;
	//double az;
	struct adis16488_msg_s data;
	//int ret;
	//long test = 0;

	printf("open /dev/imu0\n");
	fd = open("/dev/imu0",O_RDWR);

	if(fd<0){
		printf("error opening /dev/imu0\n");
		return -1;
	}

	printf("opened /dev/imu0\n");


	for (int i=0; i<1000; i++){
		read(fd,&data,sizeof(data));
		printf("%4d ax: %10d ay: %10d az: %10d\n",i, data.x_accl, data.y_accl, data.z_accl);

		iax=data.x_accl;
		iay=data.y_accl;
		iaz=data.z_accl;

		printf("%4d ax: %8.4lf ay: %8.4lf az: %8.4lf\n",i, 0.125e-7*(double)iax, 0.125e-7*(double)iay, 0.125e-7*(double)iaz);
usleep(500000);
	}

	close(fd);


  return 0;
}
