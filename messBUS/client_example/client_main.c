/****************************************************************************
 * messBUS/client_main
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/ioctl.h>

#include <nuttx/messBUS/messBUSClient.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define AUTO_SWITCH_SLOTLISTS	1
#define PRINT_DATA				1
#define GET_PHASE				0

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int client_main(int argc, char *argv[])
#endif
{
  int fd;
  int ret;
  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;
  struct slotlist1_s slotlist0 = {0};
  struct slotlist1_s slotlist1 = {0};
  int8_t phase = 0;

  char rxbuffer0[20] = "Err";
  char rxbuffer1[20] = "Err";
  char txbuffer0[20] = "Cl1";
  char txbuffer1[20] = "Cl2";
  uint16_t ndtr0 = 3;
  uint16_t ndtr1 = 3;

 /* Slotlist0 containing Slots 0-3 */
  slotlist0.n_slots = 4;
  slotlist0.slot[0].baud = 4000000;
  slotlist0.slot[0].buf_length = 0;
  slotlist0.slot[0].buffer = rxbuffer0;
  slotlist0.slot[0].read_write = MESSBUS_RX;
  slotlist0.slot[0].t_start = 10;
  slotlist0.slot[0].t_end = 45;

  slotlist0.slot[1].baud = 4000000;
  slotlist0.slot[1].buf_length = ndtr0;
  slotlist0.slot[1].buffer = txbuffer0;
  slotlist0.slot[1].read_write = MESSBUS_TX;
  slotlist0.slot[1].t_start = 1000;
  slotlist0.slot[1].t_end = 1016;

  slotlist0.slot[2].baud = 10000000;
  slotlist0.slot[2].buf_length = ndtr1;
  slotlist0.slot[2].buffer = txbuffer1;
  slotlist0.slot[2].read_write = MESSBUS_TX;
  slotlist0.slot[2].t_start = 1017;
  slotlist0.slot[2].t_end = 1029;

  slotlist0.slot[3].baud = 4000000;
  slotlist0.slot[3].buf_length = 0;
  slotlist0.slot[3].buffer = rxbuffer1;
  slotlist0.slot[3].read_write = MESSBUS_RX;
  slotlist0.slot[3].t_start = 9960;
  slotlist0.slot[3].t_end = 9993;

  /* Slotlist1 containing Slots 0-3 */
  slotlist1.n_slots = 4;
  slotlist1.slot[0].baud = 4000000;
  slotlist1.slot[0].buf_length = 0;
  slotlist1.slot[0].buffer = rxbuffer0;
  slotlist1.slot[0].read_write = MESSBUS_RX;
  slotlist1.slot[0].t_start = 10;
  slotlist1.slot[0].t_end = 45;

  slotlist1.slot[1].baud = 4000000;
  slotlist1.slot[1].buf_length = ndtr0;
  slotlist1.slot[1].buffer = txbuffer0;
  slotlist1.slot[1].read_write = MESSBUS_TX;
  slotlist1.slot[1].t_start = 1000;
  slotlist1.slot[1].t_end = 1016;

  slotlist1.slot[2].baud = 4000000;
  slotlist1.slot[2].buf_length = ndtr1;
  slotlist1.slot[2].buffer = txbuffer1;
  slotlist1.slot[2].read_write = MESSBUS_TX;
  slotlist1.slot[2].t_start = 1017;
  slotlist1.slot[2].t_end = 1029;

  slotlist1.slot[3].baud = 4000000;
  slotlist1.slot[3].buf_length = 0;
  slotlist1.slot[3].buffer = rxbuffer1;
  slotlist1.slot[3].read_write = MESSBUS_RX;
  slotlist1.slot[3].t_start = 9960;
  slotlist1.slot[3].t_end = 9993;

  /* Attach slotlist0 to the slotlist container */
  container.slotlist1 = &slotlist0;

  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusClient", O_RDWR);
  if (fd < 0)
      {
	  errcode = errno;
	  printf("hello_main: open failed: %d\n", errcode);
      }

  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
  ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
	  }

   /* Start the messBUSClient via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
   ret = ioctl(fd, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("hello_main: Ioctl start failed: %d\n", errcode);
   	  }
#if AUTO_SWITCH_SLOTLISTS
   /* Now loop forever */
   for(;;)
   {
	/* Wait until a new time slice has begun.
	 * In order not to miss new time slices CONFIG_USEC_PER_TICK should not
	 * exceed 1000 (1ms) by much as this ioctl command is in fact a while
	 * loop calling usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP) until a new
	 * time slice has begun.
	 */
	ret = ioctl(fd, MESSBUSIOC_SYNCWAIT, 0);
	if (ret < 0)
	     {
	   	 errcode = errno;
	   	 printf("hello_main: Ioctl syncwait failed: %d\n", errcode);
	   	 }
	else
		counter++;

	/* After 100 timeslices change the slotlist again and reset the counter */
	if (counter > 99)
	{
		counter = 0;
		if (activeslotlist)
		{
			container.slotlist1 = &slotlist0;
			activeslotlist = 0;
		}
		else
		{
			container.slotlist1 = &slotlist1;
			activeslotlist = 1;
		}

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
			  }
	}
#ifdef CONFIG_ARCH_CHIP_STM32F7
	/* If using a STM32F7 architecture always call the ioctl command
	 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
	 * This has to be done because the CPU's cache might not be
	 * coherent with the new data of the rxbuffers in the SRAM.
	 */
	ret = ioctl(fd, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl cache failed: %d\n", errcode);
		 }
#endif

#if GET_PHASE
	ret = ioctl(fd, MESSBUSIOC_GET_PHASE_DEVIATION, (unsigned long) &phase);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl get phase failed: %d\n", errcode);
		 }
#endif

#if PRINT_DATA
	/* Printf our data */
	printf("%d\n", counter);
	printf("%d \n", phase);
	printf("%s \n", rxbuffer0);
	printf("%s \n", rxbuffer1);
	printf("%d \n", slotlist0.slot[0].buf_length);
	printf("%d \n", slotlist0.slot[3].buf_length);
#endif

   }

/* We should never reach this point */
#endif
  return 0;
}
