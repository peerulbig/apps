/****************************************************************************
 * messBUS/master_main
 *
 *   Author: Peer Ulbig <p.ulbig@tu-braunschweig.de>
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <nuttx/board.h>

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/boardctl.h>

#include <nuttx/messBUS/messBUSMaster.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Set this #define to 1 if this app is the application entrypoint in NuttX. */
#define AUTOSTART				0

/* Loop forever and switch slotlists in this loop. Maybe also print the data
 * in the console. */
#define AUTO_SWITCH_SLOTLISTS	1
#define PRINT_DATA				0

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * master_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int master_main(int argc, char *argv[])
#endif
{
  int fd;
  int ret;
  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;

  char rxbuffer0[20] = "Err";
  char rxbuffer1[20] = "Err";
  char txbuffer0[20] = "MASTER";
  char txbuffer1[20] = "Ma1";
  uint16_t buflength0 = 6;
  uint16_t buflength1 = 3;

#if CONFIG_MESSBUS_USE_CH1
  struct slotlist1_s slotlist10 = {0};
  struct slotlist1_s slotlist11 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH2
  struct slotlist2_s slotlist20 = {0};
  struct slotlist2_s slotlist21 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH3
  struct slotlist3_s slotlist30 = {0};
  struct slotlist3_s slotlist31 = {0};
#endif
#if CONFIG_MESSBUS_USE_CH4
  struct slotlist4_s slotlist40 = {0};
  struct slotlist4_s slotlist41 = {0};
#endif

#if CONFIG_MESSBUS_USE_CH1
 /* Slotlist10 containing Slots 0-3 */
  slotlist10.n_slots = 4;
  slotlist10.slot[0].baud = 4000000;
  slotlist10.slot[0].buf_length = buflength0;
  slotlist10.slot[0].buffer = txbuffer0;
  slotlist10.slot[0].read_write = MESSBUS_TX;
  slotlist10.slot[0].t_start = 10;
  slotlist10.slot[0].t_end = 28;

  slotlist10.slot[1].baud = 10000000;
  slotlist10.slot[1].buf_length = 0;
  slotlist10.slot[1].buffer = rxbuffer0;
  slotlist10.slot[1].read_write = MESSBUS_RX;
  slotlist10.slot[1].t_start = 32;
  slotlist10.slot[1].t_end = 40;

  slotlist10.slot[2].baud = 4000000;
  slotlist10.slot[2].buf_length = 0;
  slotlist10.slot[2].buffer = rxbuffer1;
  slotlist10.slot[2].read_write = MESSBUS_RX;
  slotlist10.slot[2].t_start = 45;
  slotlist10.slot[2].t_end = 51;

  slotlist10.slot[3].baud = 10000000;
  slotlist10.slot[3].buf_length = buflength1;
  slotlist10.slot[3].buffer = txbuffer1;
  slotlist10.slot[3].read_write = MESSBUS_TX;
  slotlist10.slot[3].t_start = 56;
  slotlist10.slot[3].t_end = 62;

  /* Slotlist11 containing Slots 0-3 */
  slotlist11.n_slots = 4;
  slotlist11.slot[0].baud = 10000000;
  slotlist11.slot[0].buf_length = buflength0;
  slotlist11.slot[0].buffer = txbuffer0;
  slotlist11.slot[0].read_write = MESSBUS_TX;
  slotlist11.slot[0].t_start = 10;
  slotlist11.slot[0].t_end = 28;

  slotlist11.slot[1].baud = 10000000;
  slotlist11.slot[1].buf_length = 0;
  slotlist11.slot[1].buffer = rxbuffer0;
  slotlist11.slot[1].read_write = MESSBUS_RX;
  slotlist11.slot[1].t_start = 32;
  slotlist11.slot[1].t_end = 40;

  slotlist11.slot[2].baud = 4000000;
  slotlist11.slot[2].buf_length = 0;
  slotlist11.slot[2].buffer = rxbuffer1;
  slotlist11.slot[2].read_write = MESSBUS_RX;
  slotlist11.slot[2].t_start = 45;
  slotlist11.slot[2].t_end = 51;

  slotlist11.slot[3].baud = 10000000;
  slotlist11.slot[3].buf_length = buflength1;
  slotlist11.slot[3].buffer = txbuffer1;
  slotlist11.slot[3].read_write = MESSBUS_TX;
  slotlist11.slot[3].t_start = 56;
  slotlist11.slot[3].t_end = 62;
#endif

#if CONFIG_MESSBUS_USE_CH4
 /* Slotlist40 containing Slots 0-3 */
  slotlist40.n_slots = 4;
  slotlist40.slot[0].baud = 4000000;
  slotlist40.slot[0].buf_length = buflength0;
  slotlist40.slot[0].buffer = txbuffer0;
  slotlist40.slot[0].read_write = MESSBUS_TX;
  slotlist40.slot[0].t_start = 10;
  slotlist40.slot[0].t_end = 28;

  slotlist40.slot[1].baud = 10000000;
  slotlist40.slot[1].buf_length = 0;
  slotlist40.slot[1].buffer = rxbuffer0;
  slotlist40.slot[1].read_write = MESSBUS_RX;
  slotlist40.slot[1].t_start = 32;
  slotlist40.slot[1].t_end = 40;

  slotlist40.slot[2].baud = 4000000;
  slotlist40.slot[2].buf_length = 0;
  slotlist40.slot[2].buffer = rxbuffer1;
  slotlist40.slot[2].read_write = MESSBUS_RX;
  slotlist40.slot[2].t_start = 45;
  slotlist40.slot[2].t_end = 51;

  slotlist40.slot[3].baud = 4000000;
  slotlist40.slot[3].buf_length = buflength1;
  slotlist40.slot[3].buffer = txbuffer1;
  slotlist40.slot[3].read_write = MESSBUS_TX;
  slotlist40.slot[3].t_start = 56;
  slotlist40.slot[3].t_end = 62;

  /* Slotlist41 containing Slots 0-3 */
  slotlist41.n_slots = 4;
  slotlist41.slot[0].baud = 4000000;
  slotlist41.slot[0].buf_length = buflength0;
  slotlist41.slot[0].buffer = txbuffer0;
  slotlist41.slot[0].read_write = MESSBUS_TX;
  slotlist41.slot[0].t_start = 10;
  slotlist41.slot[0].t_end = 28;

  slotlist41.slot[1].baud = 10000000;
  slotlist41.slot[1].buf_length = 0;
  slotlist41.slot[1].buffer = rxbuffer0;
  slotlist41.slot[1].read_write = MESSBUS_RX;
  slotlist41.slot[1].t_start = 32;
  slotlist41.slot[1].t_end = 40;

  slotlist41.slot[2].baud = 4000000;
  slotlist41.slot[2].buf_length = 0;
  slotlist41.slot[2].buffer = rxbuffer1;
  slotlist41.slot[2].read_write = MESSBUS_RX;
  slotlist41.slot[2].t_start = 45;
  slotlist41.slot[2].t_end = 51;

  slotlist41.slot[3].baud = 10000000;
  slotlist41.slot[3].buf_length = buflength1;
  slotlist41.slot[3].buffer = txbuffer1;
  slotlist41.slot[3].read_write = MESSBUS_TX;
  slotlist41.slot[3].t_start = 56;
  slotlist41.slot[3].t_end = 62;
#endif

  /* Attach slotlist0 to the slotlist container */
#if CONFIG_MESSBUS_USE_CH1
  container.slotlist1 = &slotlist10;
#endif
#if CONFIG_MESSBUS_USE_CH2
  container.slotlist2 = &slotlist20;
#endif
#if CONFIG_MESSBUS_USE_CH3
  container.slotlist3 = &slotlist30;
#endif
#if CONFIG_MESSBUS_USE_CH4
  container.slotlist4 = &slotlist40;
#endif

  /* If this app is selected as CONFIG_USER_ENTRYPOINT the registration
   * of the driver may happens too late for the F7 boards under NuttX.
   * A quick and dirty workaround is to call the boardioc_init command
   * ourselves.
   */
#if defined(CONFIG_ARCH_CHIP_STM32F7) && AUTOSTART
  (void)boardctl(BOARDIOC_INIT, 0);
#endif

  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusMaster", O_RDWR);
  if (fd < 0)
      {
        printf("ERROR 1\n");
      }

  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
  ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
	  }

   /* Start the messBUSClient via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
   ret = ioctl(fd, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("master_main: Ioctl start failed: %d\n", errcode);
   	  }


   /* Now loop forever */
   for(;;)
   {
#if AUTO_SWITCH_SLOTLISTS
	/* Wait until a new time slice has begun.
	 * In order not to miss new time slices CONFIG_USEC_PER_TICK should not
	 * exceed 1000 (1ms) by much as this ioctl command is in fact a while
	 * loop calling usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP) until a new
	 * time slice has begun.
	 */
	ret = ioctl(fd, MESSBUSIOC_SYNCWAIT, 0);
	if (ret < 0)
	     {
	   	 errcode = errno;
	   	 printf("master_main: Ioctl syncwait failed: %d\n", errcode);
	   	 }
	else
		counter++;

	/* After 100 timeslices change the slotlist again and reset the counter */
	if (counter > 99)
	{
		counter = 0;
		if (activeslotlist)
		{
			#if CONFIG_MESSBUS_USE_CH1
			  container.slotlist1 = &slotlist10;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			  container.slotlist2 = &slotlist20;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			  container.slotlist3 = &slotlist30;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			  container.slotlist4 = &slotlist40;
			#endif
			activeslotlist = 0;
		}
		else
		{
			#if CONFIG_MESSBUS_USE_CH1
			  container.slotlist1 = &slotlist11;
			#endif
			#if CONFIG_MESSBUS_USE_CH2
			  container.slotlist2 = &slotlist21;
			#endif
			#if CONFIG_MESSBUS_USE_CH3
			  container.slotlist3 = &slotlist31;
			#endif
			#if CONFIG_MESSBUS_USE_CH4
			  container.slotlist4 = &slotlist41;
			#endif
			activeslotlist = 1;
		}

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("master_main: Ioctl slotlists failed: %d\n", errcode);
			  }
	}

	/* If using a STM32F7 architecture always call the ioctl command
	 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
	 * This has to be done because the CPU's cache might not be
	 * coherent with the new data of the rxbuffers in the SRAM.
	 *
	 * You can also call this ioctl command for other architectures,
	 * but it will have no effect then.
	 */
	ret = ioctl(fd, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("master_main: Ioctl cache failed: %d\n", errcode);
		 }

#if PRINT_DATA
	/* Printf our data */
	printf("%d\n", counter);
	printf("%s \n", rxbuffer0);
	printf("%s \n", rxbuffer1);
	printf("%d \n", slotlist0.slot[0].buf_length);
	printf("%d \n", slotlist0.slot[3].buf_length);
#endif
#endif // AUTO_SWITCH_SLOTLISTS
   }

/* We should never reach this point */

  return 0;
}
