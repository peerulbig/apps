/****************************************************************************
 * examples/hello/hello_main.c
 *
 *   Copyright (C) 2008, 2011-2012 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/ioctl.h>

#include <nuttx/messBUS/messBUSMaster.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

#define MASTER	0

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * hello_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int hello_main(int argc, char *argv[])
#endif
{
  int fd;
  int fddummy;
  int ret;
  int errcode;
  int counter = 0;
  int activeslotlist = 0;
  struct slotlists_container_s container;
  struct slotlist1_s slotlist0 = {0};
  struct slotlist1_s slotlist1 = {0};

#if MASTER
  char rxbuffer0[20] = "buf0: Error!";
  char rxbuffer1[20] = "buf1: Error!";
  char txbuffer0[20] = "MASTER";
  char txbuffer1[20] = "more txdata";
  uint16_t buflength0 = 6;
  uint16_t buflength1 = 11;

 /* Slotlist0 containing Slots 0-3 */
  slotlist0.n_slots = 4;
  slotlist0.slot[0].baud = 4000000;
  slotlist0.slot[0].buf_length = buflength0;
  slotlist0.slot[0].buffer = txbuffer0;
  slotlist0.slot[0].read_write = MESSBUS_TX;
  slotlist0.slot[0].t_start = 10;
  slotlist0.slot[0].t_end = 27;

  slotlist0.slot[1].baud = 10000000;
  slotlist0.slot[1].buf_length = 0;
  slotlist0.slot[1].buffer = rxbuffer0;
  slotlist0.slot[1].read_write = MESSBUS_RX;
  slotlist0.slot[1].t_start = 29;
  slotlist0.slot[1].t_end = 32;

  slotlist0.slot[2].baud = 4000000;
  slotlist0.slot[2].buf_length = 0;
  slotlist0.slot[2].buffer = rxbuffer1;
  slotlist0.slot[2].read_write = MESSBUS_RX;
  slotlist0.slot[2].t_start = 90;
  slotlist0.slot[2].t_end = 140;

  slotlist0.slot[3].baud = 10000000;
  slotlist0.slot[3].buf_length = buflength1;
  slotlist0.slot[3].buffer = txbuffer1;
  slotlist0.slot[3].read_write = MESSBUS_TX;
  slotlist0.slot[3].t_start = 160;
  slotlist0.slot[3].t_end = 200;

  /* Slotlist1 containing Slots 0-3 */
  slotlist1.n_slots = 4;
  slotlist1.slot[0].baud = 4000000;
  slotlist1.slot[0].buf_length = 0;
  slotlist1.slot[0].buffer = rxbuffer0;
  slotlist1.slot[0].read_write = MESSBUS_RX;
  slotlist1.slot[0].t_start = 10;
  slotlist1.slot[0].t_end = 45;

  slotlist1.slot[1].baud = 4000000;
  slotlist1.slot[1].buf_length = 0;
  slotlist1.slot[1].buffer = txbuffer0;
  slotlist1.slot[1].read_write = MESSBUS_TX;
  slotlist1.slot[1].t_start = 1000;
  slotlist1.slot[1].t_end = 1016;

  slotlist1.slot[2].baud = 4000000;
  slotlist1.slot[2].buf_length = 0;
  slotlist1.slot[2].buffer = txbuffer1;
  slotlist1.slot[2].read_write = MESSBUS_TX;
  slotlist1.slot[2].t_start = 1017;
  slotlist1.slot[2].t_end = 1029;

  slotlist1.slot[3].baud = 4000000;
  slotlist1.slot[3].buf_length = 0;
  slotlist1.slot[3].buffer = rxbuffer1;
  slotlist1.slot[3].read_write = MESSBUS_RX;
  slotlist1.slot[3].t_start = 9960;
  slotlist1.slot[3].t_end = 9980;

  /* Attach slotlist0 to the slotlist container */
  container.slotlist1 = &slotlist0;

  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusMaster", O_RDWR);
  if (fd < 0)
      {
        printf("ERROR 1\n");
      }

  /* Open the messBUSClient device again (which should fail) */
  fddummy = open("/dev/messBusMaster", O_RDWR);
  if (fddummy < 0)
  	  {
	  errcode = errno;
	  printf("hello_main: Open failed: %d\n", errcode);
	  }
#else
  char rxbuffer0[20] = "buf0: Error!";
  char rxbuffer1[20] = "buf1: Error!";
  char txbuffer0[20] = "txdata";
  char txbuffer1[20] = "more txdata";
  uint16_t ndtr0 = 6;
  uint16_t ndtr1 = 11;

 /* Slotlist0 containing Slots 0-3 */
  slotlist0.n_slots = 4;
  slotlist0.slot[0].baud = 4000000;
  slotlist0.slot[0].buf_length = 0;
  slotlist0.slot[0].buffer = rxbuffer0;
  slotlist0.slot[0].read_write = MESSBUS_RX;
  slotlist0.slot[0].t_start = 10;
  slotlist0.slot[0].t_end = 45;

  slotlist0.slot[1].baud = 4000000;
  slotlist0.slot[1].buf_length = ndtr0;
  slotlist0.slot[1].buffer = txbuffer0;
  slotlist0.slot[1].read_write = MESSBUS_TX;
  slotlist0.slot[1].t_start = 1000;
  slotlist0.slot[1].t_end = 1016;

  slotlist0.slot[2].baud = 10000000;
  slotlist0.slot[2].buf_length = ndtr1;
  slotlist0.slot[2].buffer = txbuffer1;
  slotlist0.slot[2].read_write = MESSBUS_TX;
  slotlist0.slot[2].t_start = 1017;
  slotlist0.slot[2].t_end = 1029;

  slotlist0.slot[3].baud = 4000000;
  slotlist0.slot[3].buf_length = 0;
  slotlist0.slot[3].buffer = rxbuffer1;
  slotlist0.slot[3].read_write = MESSBUS_RX;
  slotlist0.slot[3].t_start = 9960;
  slotlist0.slot[3].t_end = 9980;

  /* Slotlist1 containing Slots 0-3 */
  slotlist1.n_slots = 4;
  slotlist1.slot[0].baud = 4000000;
  slotlist1.slot[0].buf_length = 0;
  slotlist1.slot[0].buffer = rxbuffer0;
  slotlist1.slot[0].read_write = MESSBUS_RX;
  slotlist1.slot[0].t_start = 10;
  slotlist1.slot[0].t_end = 45;

  slotlist1.slot[1].baud = 4000000;
  slotlist1.slot[1].buf_length = ndtr0;
  slotlist1.slot[1].buffer = txbuffer0;
  slotlist1.slot[1].read_write = MESSBUS_TX;
  slotlist1.slot[1].t_start = 1000;
  slotlist1.slot[1].t_end = 1016;

  slotlist1.slot[2].baud = 4000000;
  slotlist1.slot[2].buf_length = ndtr1;
  slotlist1.slot[2].buffer = txbuffer1;
  slotlist1.slot[2].read_write = MESSBUS_TX;
  slotlist1.slot[2].t_start = 1017;
  slotlist1.slot[2].t_end = 1029;

  slotlist1.slot[3].baud = 4000000;
  slotlist1.slot[3].buf_length = 0;
  slotlist1.slot[3].buffer = rxbuffer1;
  slotlist1.slot[3].read_write = MESSBUS_RX;
  slotlist1.slot[3].t_start = 9960;
  slotlist1.slot[3].t_end = 9980;

  /* Attach slotlist0 to the slotlist container */
  container.slotlist1 = &slotlist0;

  /* Open the messBUSClient device and save the file descriptor */
  fd = open("/dev/messBusClient", O_RDWR);
  if (fd < 0)
      {
        printf("ERROR 1\n");
      }

  /* Open the messBUSClient device again (which should fail) */
  fddummy = open("/dev/messBusClient", O_RDWR);
  if (fddummy < 0)
  	  {
	  errcode = errno;
	  printf("hello_main: Open failed: %d\n", errcode);
	  }
#endif

  /* Attach a slotlist container to the file descriptor (which
   * represents our messBUSClient device) via ioctl.
   */
  ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
  if (ret < 0)
	  {
	  errcode = errno;
	  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
	  }

   /* Start the messBUSClient via ioctl.
    * Starting the messBUS always requires a previous call of the ioctl
    * command MESSBUSIOC_ATTACH_SLOTLISTS. Otherwise the ioctl command
    * MESSBUSIOC_START will fail, because the messBUS does not know what
    * to do when running.
    */
   ret = ioctl(fd, MESSBUSIOC_START, 0);
   if (ret < 0)
      {
   	  errcode = errno;
   	  printf("hello_main: Ioctl start failed: %d\n", errcode);
   	  }
#if 0
   /* Now loop forever */
   for(;;)
   {
	/* Wait until a new time slice has begun.
	 * In order not to miss new time slices CONFIG_USEC_PER_TICK should not
	 * exceed 1000 (1ms) by much as this ioctl command is in fact a while
	 * loop calling usleep(CONFIG_MESSBUS_SYNCWAIT_SLEEP) until a new
	 * time slice has begun.
	 */
	ret = ioctl(fd, MESSBUSIOC_SYNCWAIT, 0);
	if (ret < 0)
	     {
	   	 errcode = errno;
	   	 printf("hello_main: Ioctl syncwait failed: %d\n", errcode);
	   	 }
	else
		counter++;

	/* After 100 timeslices change the slotlist again and reset the counter */
	if (counter > 99)
	{
		counter = 0;
		if (activeslotlist)
		{
			container.slotlist1 = &slotlist0;
			activeslotlist = 0;
		}
		else
		{
			container.slotlist1 = &slotlist1;
			activeslotlist = 1;
		}

		/* Attach the new slotlist container to the messBUS device */
		ret = ioctl(fd, MESSBUSIOC_ATTACH_SLOTLISTS, (unsigned long) &container);
		if (ret < 0)
			  {
			  errcode = errno;
			  printf("hello_main: Ioctl slotlists failed: %d\n", errcode);
			  }
	}
#if CONFIG_ARCH_CHIP_STM32F7
	/* If using a STM32F7 architecture always call the ioctl command
	 * MESSBUSIOC_UPDATE_RXBUFFERS before accessing the rx buffers.
	 * This has to be done because the CPU's cache might not be
	 * coherent with the new data of the rxbuffers in the SRAM.
	 */
	ret = ioctl(fd, MESSBUSIOC_UPDATE_RXBUFFERS, 0);
	if (ret < 0)
		 {
		 errcode = errno;
		 printf("hello_main: Ioctl cache failed: %d\n", errcode);
		 }
#endif

	/* Printf our data */
	printf("%d\n", counter);
	printf("%s \n", rxbuffer0);
	printf("%s \n", rxbuffer1);
	printf("%d \n", slotlist0.slot[0].buf_length);
	printf("%d \n", slotlist0.slot[3].buf_length);

   }
#endif

/* We should never reach this point */
  return 0;
}
