/*
 * control.h
 *
 *  Created on: 15.10.2013
 *      Author: snowak
 */

#ifndef CONTROL_H_
#define CONTROL_H_

#include <sys/types.h>
//#include "stm32f4xx.h" //uint8_t ... ???
//#include <stddef.h>   //NULL???
void SpeedControlSetPID(float p, float i, float d);
void SpeedControlGetPID(float *p, float *i, float *d);
//void SpeedControlStep(float SpeedMean, float SpeedDiff, int16_t ODO_Cnt[4], uint8_t disable_flag, int8_t *powersetting);
void SpeedControlStep( float SpeedLeft, float SpeedRight, int16_t ODO_Cnt[4], uint8_t disable_flag, int8_t *powersetting);

#endif /* CONTROL_H_ */
